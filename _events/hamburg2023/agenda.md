---
layout: event_detail
title: Agenda
event: hamburg2023
order: 10
permalink: /events/hamburg2023/agenda/
---

Reproducible Builds Summit 2023

The following was the schedule for the 2023 Reproducible Builds Summit in Hamburg, Germany.

Day 1 - Tuesday, October 31
----------------------------

* 9.30 Opening Circle
* 10.00 Project updates
  * [SUSE and openSUSE]({{ "/events/hamburg2023/suse-opensuse-updates/" | relative_url }})
  * Rebuilding binaries (for real)
	* LINK(S)
  * Fedora
	* LINK(S)
  * Debian
	* LINK(S)
  * ElectroBSD
	* LINK(S)
  * Reproducible Central
	* LINK(S)
  * NixOS
	* [minimal installation ISO](https://discourse.nixos.org/t/nixos-reproducible-builds-minimal-installation-iso-successfully-independently-rebuilt/34756)

* 11.15 Break
* 11.00 [Mapping the Big Picture]({{ "/events/hamburg2023/big-picture/" | relative_url }})
* 13.00 Lunch
* 14.00 Collaborative Working Sessions
  * [Towards a snapshot service]({{ "/events/hamburg2023/snapshot-service/" | relative_url }})
  * [Understanding user-facing needs and personas]({{ "/events/hamburg2023/users/" | relative_url }})
  * [Language-specific package managers]({{ "/events/hamburg2023/language-specific/" | relative_url }})
  * [Defining our definitions]({{ "/events/hamburg2023/definitions/" | relative_url }})
* 15.15 Break
* 15.30 Collaborative Working Sessions/Hack Time
* 16.30 Closing Circle
* 17.00 Adjourn

Day 2 - Wednesday, November 1st TODO
-------------------------------

* 9.30 Opening Circle
  * The day started with a summary of Day 1 outcomes and a Day 2 Agenda Overview.
* 9.45 Collaborative Working Sessions, break-out discussions continue.
  * [Ten Commandments]({{ "/events/hamburg2023/rb-commandments/" | relative_url }})
  * [Embedded systems]({{ "/events/hamburg2023/embedded-systems//" | relative_url }})
  * [Guix To-do's]({{ "/events/hamburg2023/guix-todo/" | relative_url }})
  * [Signature storage and sharing]({{ "/events/hamburg2023/signature-storage/" | relative_url }})
  * [Public verification service]({{ "/events/hamburg2023/verification1/" | relative_url }})
* 11.15 Break
* 11.30 Participant Skill Share
  * Participants were encouraged to share any skill the consider relevant to the meeting scope. The session was structured so as to minimize group size and maximize 1-on-1 sharing opportunities.
* 13.00 Lunch
* 14.00 Collaborative Working Sessions
  * [Verification use cases]({{ "/events/hamburg2023/verification2/" | relative_url }})
  * [Web site audiences]({{ "/events/hamburg2023/site-audiences/" | relative_url }})
  * [Born Reproducible I]({{ "/events/hamburg2023/born-reproducible-1/" | relative_url }})
  * [RB Success Stories]({{ "/events/hamburg2023/success/" | relative_url }})
  * RB relationship to SBOM
* 15.15 Break
* 15:30 Hacking Time
* 16.35 Closing Circle
* 17.00 Adjourn

Day 3 - Thursday, November 2nd
------------------------------

* 9.00 Opening Circle
  * The day started with a summary of Day 2 outcomes and a Day 3 Agenda Overview.
* 9.45 Collaborative Working Sessions, break-out discussions continue.
  * [SBOM for rpm]({{ "/events/hamburg2023/rpm-sbom/" | relative_url }})
  * [Filtering diffoscope output]({{ "/events/hamburg2023/filtering-diffoscope/" | relative_url }})
  * [Images/Filesystems/Containers]({{ "/events/hamburg2023/images-filesystems/" | relative_url }})
  * [Born Reproducible II]({{ "/events/hamburg2023/born-reproducible-2/" | relative_url }})
  * [Using verification data]({{ "/events/hamburg2023/verification2/" | relative_url }})
* 11.15 Break
* 11.30 Collaborative Working Sessions, break-out discussions continue.
  * [Born Reproducible III]({{ "/events/hamburg2023/born-reproducible-3/" | relative_url }})
  * [Verification Service III]({{ "/events/hamburg2023/verification2/" | relative_url }})
  * [Fedora packages]({{ "/events/hamburg2023/fedora-packages/" | relative_url }})
  * [Arch huddle]({{ "/events/hamburg2023/arch-huddle/" | relative_url }})
  * [Diffoscope II]({{ "/events/hamburg2023/diffoscope-2/" | relative_url }})
  * [Debian]({{ "/events/hamburg2023/debian/" | relative_url }})
* 12.30 Lunch
* 14.00 Mapping next conversations and next steps
  * The group paused before the final session to take stock of the progress made to this point in the week and to inventory action items, next steps and other bridges to post-event collaboration.
* 15.30 Closing Circle
  * Participants summarized key outcomes from the event, and discussed next steps for continuing collaboration after the meeting.
* 16.00 Adjourn / Hack Time


