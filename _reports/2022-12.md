---
layout: report
year: "2022"
month: "12"
title: "Reproducible Builds in December 2022"
draft: false
date: 2023-01-07 15:22:28
---

[![]({{ "/images/reports/2022-12/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the December 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project.**

---

[![]({{ "/images/reports/2022-12/summit_photo.jpg#right" | relative_url }})]({{ "/events/hamburg2023/" | relative_url }})

We are extremely pleased to announce that the dates for the [Reproducible Builds Summit in 2023]({{ "/events/hamburg2023/" | relative_url }}) have been announced in 2022 already:

* When: October 31st, November 1st, November 2nd 2023.
* Where: [*Dock Europe*](https://dock-europe.net/), Hamburg, Germany.

We plan to spend three days continuing to the grow of the Reproducible Builds effort. As in previous events, the exact content of the meeting will be shaped by the participants. And, as mentioned in [Holger Levsen's post to our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2022-December/002804.html), the dates have been booked and confirmed with the venue, so if you are considering attending, **please reserve these dates** in your calendar today.

---

[![]({{ "/images/reports/2022-12/nixos.png#right" | relative_url }})](https://remy.grunblatt.org/nix-and-nixos-my-pain-points.html)

[Rémy Grünblatt](https://remy.grunblatt.org/), an associate professor in the [Télécom Sud-Paris](https://telecom-sudparis.eu/) engineering school wrote up his ["pain points" of using Nix and NixOS](https://remy.grunblatt.org/nix-and-nixos-my-pain-points.html). Although some of the points do not touch on reproducible builds, Rémy touches on problems he has encountered with the different kinds of reproducibility that these distributions appear to promise including configuration files affecting the behaviour of systems, the fragility of upstream sources as well as the conventional idea of binary reproducibility.

---

[![]({{ "/images/reports/2022-12/golang.png#right" | relative_url }})](https://go.dev/)

Morten Linderud reported that he is quietly optimistic that if [Go programming language](https://go.dev) resolves all of its issues with reproducible builds ([tracking issue](https://github.com/golang/go/issues/57120)) then the Go binaries distributed from Google and by [Arch Linux](https://archlinux.org/) may be bit-for-bit identical. "It's just a bit early to sorta figure out what roadblocks there are. [But] Go bootstraps itself every build, so in theory I think it should be possible."


---

[![]({{ "/images/reports/2022-12/dwheeler-2003c.jpg#right" | relative_url }})]({{ "/news/2022/12/15/supporter-spotlight-davidawheeler-supply-chain-security/" | relative_url }}")

On December 15th, Holger Levsen published an [in-depth interview he performed with David A. Wheeler]({{ "/news/2022/12/15/supporter-spotlight-davidawheeler-supply-chain-security/" | relative_url }}) on supply-chain security and reproducible builds, but it also touches on the biggest challenges in computing as well.

This is part of a larger series of posts featuring the projects, companies and individuals who support the Reproducible Builds project. Other instalments include an article [featuring the Civil Infrastructure Platform]({{ "/news/2020/10/21/supporter-spotlight-cip-project/" | relative_url }}) project and followed this up with a [post about the Ford Foundation]({{ "/news/2021/04/06/supporter-spotlight-ford-foundation/" | relative_url }}) as well as a recent ones about [ARDC]({{ "/news/2022/04/14/supporter-spotlight-ardc/" | relative_url }}), the [Google Open Source Security Team (GOSST)]({{ "/news/2022/04/26/supporter-spotlight-google-open-source-security-team/" | relative_url }}), [Jan Nieuwenhuizen on Bootstrappable Builds, GNU Mes and GNU Guix]({{ "/news/2022/05/18/jan-nieuwenhuizen-on-bootrappable-builds-gnu-mes-and-gnu-guix/" | relative_url }}) and [Hans-Christoph Steiner of the F-Droid project]({{ "/news/2022/06/24/supporter-spotlight-hans-christoph-steiner-f-droid-project/" | relative_url }}).

---

[![]({{ "/images/reports/2022-12/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

A number of changes were made to the Reproducible Builds website and documentation this month, including FC Stegerman adding an F-Droid/apksigcopier example to our [embedded signatures]({{ "/docs/embedded-signatures/" | relative_url }}) page&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/34817389)], Holger Levsen making a large number of changes related to the 2022 summit in Venice as well as 2023's summit in Hamburg&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6ae3578d)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/41a1779c)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0539562a)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/ba9a56a9)] and Simon Butler updated our [publications page]({{ "/docs/publications/" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6cc6f723)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/bbdda2b4)].

---

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, James Addison asked a question about whether there has been any effort to trace the files used by a build system in order to identify the corresponding build-dependency packages.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2022-December/002779.html)] In addition, Bernhard M. Wiedemann then posed a thought-provoking question asking "[How to talk to skeptics?](https://lists.reproducible-builds.org/pipermail/rb-general/2022-December/002782.html)", which was occasioned by a colleague who had [published a blog post in May 2021 skeptical of reproducible builds](https://fy.blackhats.net.au/blog/html/2021/05/12/compiler_bootstrapping_can_we_trust_rust.html). The thread generated a number of replies.

---

### Android news

[![]({{ "/images/reports/2022-12/android.png#right" | relative_url }})](https://en.wikipedia.org/wiki/Android_(operating_system))

*obfusk* (FC Stegerman) [performed a thought-provoking review](https://gist.github.com/obfusk/c51ebbf571e04ddf29e21146096675f8) of tools designed to determine the difference between two different `.apk` files shipped by a number of free-software instant messenger applications.

These scripts are often necessary in the Android/APK ecosystem due to these files containing embedded signatures so the conventional bit-for-bit comparison cannot be used. After detailing a litany of issues with these tools, they come to the conclusion that:

> It's quite possible these messengers actually have reproducible builds, but the verification scripts they use don't actually allow us to verify whether they do.

This reflects the consensus view within the Reproducible Builds project: pursuing a situation in language or package ecosystems where binaries are bit-for-bit identical (over requiring a bespoke ecosystem-specific tool) is not a luxury demanded by purist engineers, but rather the only *practical* way to demonstrate reproducibility. *obfusk* also [announced the first release of their own set of tools](https://lists.reproducible-builds.org/pipermail/rb-general/2022-December/002769.html) on our mailing list.

Related to this, *obfusk* also [posted to an issue filed against Mastodon](https://github.com/mastodon/mastodon-android/issues/4#issuecomment-1336259343) regarding the difficulties of creating bit-by-bit identical APKs, especially with respect to copying [v2/v3 APK signatures](https://source.android.com/docs/security/features/apksigning/v2) created by different tools; they also reported that some APK ordering differences were not caused by building on macOS after all, but by using [Android Studio](https://developer.android.com/studio) [[...](https://gitlab.com/fdroid/fdroiddata/-/issues/2816#note_1204147286)] and that F-Droid [added 16 more apps published with Reproducible Builds](https://gitlab.com/fdroid/fdroiddata/-/issues/2844) in December.

---

### Debian

[![]({{ "/images/reports/2022-12/debian.png#right" | relative_url }})](https://debian.org/)

As mentioned in [last months report]({{ "/reports/2022-11/" | relative_url }}), Vagrant Cascadian has been organising a series of online sprints in order to 'clear the huge backlog of reproducible builds patches submitted' by performing NMUs ([Non-Maintainer Uploads](https://wiki.debian.org/NonMaintainerUpload)).

During December, meetings were held on the 1st, 8th, 15th, 22nd and 29th, resulting in a large number of uploads and bugs being addressed:

* Chris Lamb: [`aespipe`](https://tracker.debian.org/pkg/aespipe) ([#661079](https://bugs.debian.org/661079), [#1020809](https://bugs.debian.org/1020809)), [`cdbackup`](https://tracker.debian.org/pkg/cdbackup) ([#1011428](https://bugs.debian.org/1011428)) & [`xmlrpc-epi`](https://tracker.debian.org/pkg/xmlrpc-epi) ([#865688](https://bugs.debian.org/865688), [#1020651](https://bugs.debian.org/1020651))

* Holger Levsen: [`apr-util`](https://tracker.debian.org/pkg/apr-util) ([#1006865](https://bugs.debian.org/1006865)), [`lirc`](https://tracker.debian.org/pkg/lirc) ([#979024](https://bugs.debian.org/979024)) & [`ruby-omniauth-tumblr`](https://tracker.debian.org/pkg/ruby-omniauth-tumblr)

* Vagrant Cascadian: [`amavisd-milter`](https://tracker.debian.org/pkg/amavisd-milter) ([#975954](https://bugs.debian.org/975954)), [`apophenia`](https://tracker.debian.org/pkg/apophenia) ([#940013](https://bugs.debian.org/940013)), [`cfi`](https://tracker.debian.org/pkg/cfi) ([#995647](https://bugs.debian.org/995647)), [`chessx`](https://tracker.debian.org/pkg/chessx) ([#881664](https://bugs.debian.org/881664)), [`cmocka`](https://tracker.debian.org/pkg/cmocka) ([#991181](https://bugs.debian.org/991181)), [`desmume`](https://tracker.debian.org/pkg/desmume) ([#890312](https://bugs.debian.org/890312)), [`golang-gonum-v1-plot`](https://tracker.debian.org/pkg/golang-gonum-v1-plot) ([#968045](https://bugs.debian.org/968045)), [`intel-gpu-tools`](https://tracker.debian.org/pkg/intel-gpu-tools) ([#945105](https://bugs.debian.org/945105)), [`jhbuild`](https://tracker.debian.org/pkg/jhbuild) ([#971420](https://bugs.debian.org/971420)), [`libjama`](https://tracker.debian.org/pkg/libjama) ([#986601](https://bugs.debian.org/986601)), [`libjs-qunit`](https://tracker.debian.org/pkg/libjs-qunit) ([#976445](https://bugs.debian.org/976445)), [`liblip`](https://tracker.debian.org/pkg/liblip) ([#1001513](https://bugs.debian.org/1001513), [#989583](https://bugs.debian.org/989583)), [`libstatgrab`](https://tracker.debian.org/pkg/libstatgrab) ([#961747](https://bugs.debian.org/961747)), [`mlpost`](https://tracker.debian.org/pkg/mlpost) ([#977179](https://bugs.debian.org/977179) and [#977180](https://bugs.debian.org/977180)), [`netcdf-parallel`](https://tracker.debian.org/pkg/netcdf-parallel) ([#972930](https://bugs.debian.org/972930)), [`netgen-lvs`](https://tracker.debian.org/pkg/netgen-lvs) ([#955783](https://bugs.debian.org/955783)), [`perfect-scrollbar`](https://tracker.debian.org/pkg/perfect-scrollbar) ([#1000770](https://bugs.debian.org/1000770)), [`python-tomli`](https://tracker.debian.org/pkg/python-tomli) ([#994979](https://bugs.debian.org/994979)), [`pytsk`](https://tracker.debian.org/pkg/pytsk) ([#992060](https://bugs.debian.org/992060)), [`smplayer`](https://tracker.debian.org/pkg/smplayer) ([#997689](https://bugs.debian.org/997689)), [`squeak-plugins-scratch`](https://tracker.debian.org/pkg/squeak-plugins-scratch) ([#876771](https://bugs.debian.org/876771), [#942006](https://bugs.debian.org/942006)), [`stgit`](https://tracker.debian.org/pkg/stgit) ([#942009](https://bugs.debian.org/942009)), [`strace`](https://tracker.debian.org/pkg/strace) ([#896016](https://bugs.debian.org/896016)), [`surgescript`](https://tracker.debian.org/pkg/surgescript) ([#992061](https://bugs.debian.org/992061)), [`sympow`](https://tracker.debian.org/pkg/sympow) ([#973601](https://bugs.debian.org/973601)), [`wxmaxima`](https://tracker.debian.org/pkg/wxmaxima) ([#983148](https://bugs.debian.org/983148)), [`xavs2`](https://tracker.debian.org/pkg/xavs2) ([#952493](https://bugs.debian.org/952493)), [`xaw3d`](https://tracker.debian.org/pkg/xaw3d) ([#991180](https://bugs.debian.org/991180), [#986704](https://bugs.debian.org/986704)) and [`yard`](https://tracker.debian.org/pkg/yard) ([#972668](https://bugs.debian.org/972668)).

The next sprint is [due to take place this coming Tuesday, January 10th at 16:00 UTC](https://lists.reproducible-builds.org/pipermail/rb-general/2023-January/002807.html).

---

### Upstream patches

The Reproducible Builds project attempts to fix as many currently-unreproducible packages as possible. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

  * [`OpenRGB`](https://gitlab.com/CalcProgrammer1/OpenRGB/-/merge_requests/1567) (filesystem ordering issue)
  * [`python-maturin`](https://bugzilla.opensuse.org/show_bug.cgi?id=1206342) (report an issue regarding random numbers)
  * [`rav1e`](https://github.com/xiph/rav1e/pull/3081) (datetime-related issue)
  * [`weblate`](https://github.com/WeblateOrg/weblate/issues/8556) (report that the build fails in 2038)

* Chris Lamb:

    * [#1025415](https://bugs.debian.org/1025415) filed against [`cctools`](https://tracker.debian.org/pkg/cctools).
    * [#1025801](https://bugs.debian.org/1025801) filed against [`sphinx`](https://tracker.debian.org/pkg/sphinx) ([forwarded upstream](https://github.com/sphinx-doc/sphinx/pull/11037))
    * [#1026381](https://bugs.debian.org/1026381) filed against [`python-django-health-check`](https://tracker.debian.org/pkg/python-django-health-check).
    * [#1026876](https://bugs.debian.org/1026876) filed against [`jamin`](https://tracker.debian.org/pkg/jamin).
    * [#1026877](https://bugs.debian.org/1026877) filed against [`opari2`](https://tracker.debian.org/pkg/opari2).

---

### Testing framework

[![]({{ "/images/reports/2022-12/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility. In October, the following changes were made by Holger Levsen:

* The `osuosl167` machine is no longer a `openqa-worker` node anymore.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3aaabdbd)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/64841ce1)]
* Detect problems with APT repository signatures&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b800b755)] and update a repository signing key&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fbe0179c)].
* reproducible Debian builtin-pho: improve job output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0e71f347)]
* Only install the `foot-terminfo` package on Debian systems.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/10319000)]

In addition, Mattia Rizzolo added support for the version of [*diffoscope*](https://diffoscope.org) in Debian *stretch* which doesn't support the `--timeout` flag.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/988c1e94)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1f5ae3b7)]

---

### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-12/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb made the following changes to [diffoscope](https://diffoscope.org), including preparing and uploading versions `228`, `229` and `230` to Debian:

* Fix compatibility with [`file(1)`](https://darwinsys.com/file/) version 5.43, with thanks to Christoph Biedl. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/44ebd188)]
* Skip the `test_html.py::test_diff` test if `html2text` is not installed. ([#1026034](https://bugs.debian.org/1026034))
* Update copyright years. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9ac43af7)]

In addition, Jelle van der Waa added support for [Berkeley DB](https://en.wikipedia.org/wiki/Berkeley_DB) version 6.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ab87ab6a)]

Orthogonal to this, Holger Levsen bumped the Debian [`Standards-Version`](https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-standards-version) on all of our packages, including *diffoscope*&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e980ce7c)], *strip-nondeterminism*&nbsp;[[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/4380d64)], *disorderfs*&nbsp;[[...](https://salsa.debian.org/reproducible-builds/disorderfs/commit/878de25)] and *reprotest*&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/18e4f65)].

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. You can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
