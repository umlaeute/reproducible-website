---
title: Academic publications
layout: docs
permalink: /docs/publications/
---

### Citing reproducible-builds.org

The
[CITATION.cff](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/CITATION.cff)
file is available at the root of the repository. It can be used to
generate citations in various formats using
[`cffconvert`](https://github.com/citation-file-format/cffconvert).

If you are preparing a paper or article and wish to reference the
[reproducible-builds.org](https://reproducible-builds.org) project, the
following BibTeX entry is recommended:

{% raw %}

    @misc{ReproducibleBuildsOrg,
      author = {{Reproducible Builds}},
      title = {Reproducible Builds Website},
      url = {https://reproducible-builds.org/}
    }

{% endraw %}

### Academic publications

In addition to the resources mentioned, our repository also includes a
[bibliography.bib](https://salsa.debian.org/reproducible-builds/reproducible-website/-/blob/master/bibliography.bib)
file, which contains BibTeX entries for all the academic publications listed
here. This file is continuously updated to reflect the most recent scholarly
works related to reproducible builds. It serves as a comprehensive source for
researchers and practitioners looking to cite relevant literature in their work.
The file can be found within the repository, making it easy for anyone to access
and utilize in their own scholarly writings.

- Thompson, K. (1984). Reflections on trusting trust. _Commun. ACM_,
  _27_(8), 761–763. <https://doi.org/10.1145/358198.358210>
- Wheeler, D. A. (2010). _Fully countering trusting trust through
  diverse double-compiling_. <https://arxiv.org/abs/1004.5534>
- Courtès, L. (2013). _Functional package management with guix_.
  <https://arxiv.org/abs/1305.4584>
- Courtès, L., & Wurmus, R. (2015, August).
  <span class="nocase">Reproducible and User-Controlled Software
  Environments in HPC with Guix</span>. _<span class="nocase">2nd
  International Workshop on Reproducibility in Parallel Computing
  (RepPar)</span>_. <https://inria.hal.science/hal-01161771>
- Ren, Z., Jiang, H., Xuan, J., & Yang, Z. (2018, May). Automated
  localization for unreproducible builds. _Proceedings of the 40th
  International Conference on Software Engineering_.
  <https://doi.org/10.1145/3180155.3180224>
- Tapas, N., Longo, F., Merlino, G., & Puliafito, A. (2019).
  Transparent, provenance-assured, and secure software-as-a-service.
  _2019 IEEE 18th International Symposium on Network Computing and
  Applications (NCA)_, 1–8. <https://doi.org/10.1109/NCA.2019.8935014>
- Torres-Arias, S., Afzali, H., Kuppusamy, T. K., Curtmola, R., &
  Cappos, J. (2019). In-toto: Providing farm-to-table guarantees for
  bits and bytes. _Proceedings of the 28th USENIX Conference on
  Security Symposium_, 1393–1410.
  <https://www.usenix.org/conference/usenixsecurity19/presentation/torres-arias>
- Ohm, M., Plate, H., Sykosch, A., & Meier, M. (2020). Backstabber’s
  knife collection: A review of open source software supply chain
  attacks. In _Lecture notes in computer science_ (pp. 23–43).
  Springer International Publishing.
  <https://doi.org/10.1007/978-3-030-52683-2_2>
- Navarro Leija, O. S., Shiptoski, K., Scott, R. G., Wang, B., Renner,
  N., Newton, R. R., & Devietti, J. (2020). Reproducible containers.
  _Proceedings of the Twenty-Fifth International Conference on
  Architectural Support for Programming Languages and Operating
  Systems_, 167–182. <https://doi.org/10.1145/3373376.3378519>
- Ohm, M., Sykosch, A., & Meier, M. (2020). Towards detection of
  software supply chain attacks by forensic artifacts. _Proceedings of
  the 15th International Conference on Availability, Reliability and
  Security_. <https://doi.org/10.1145/3407023.3409183>
- Lamb, C., & Zacchiroli, S. (2022). Reproducible builds: Increasing
  the integrity of software supply chains. _IEEE Software_, _39_(2),
  62–70. <https://doi.org/10.1109/MS.2021.3073045>
- Shi, Y., Wen, M., Cogo, F. R., Chen, B., & Jiang, Z. M. (2022). An
  experience report on producing verifiable builds for large-scale
  commercial systems. _IEEE Transactions on Software Engineering_,
  _48_(9), 3361–3377. <https://doi.org/10.1109/TSE.2021.3092692>
- Ren, Z., Sun, S., Xuan, J., Li, X., Zhou, Z., & Jiang, H. (2022).
  Automated patching for unreproducible builds. _Proceedings of the
  44th International Conference on Software Engineering_, 200–211.
  <https://doi.org/10.1145/3510003.3510102>
- Enck, W., & Williams, L. (2022). Top five challenges in software
  supply chain security: Observations from 30 industry and government
  organizations. _IEEE Security & Privacy_, _20_(2), 96–100.
  <https://doi.org/10.1109/MSEC.2022.3142338>
- Butler, S., Gamalielsson, J., Lundell, B., Brax, C., Mattsson, A.,
  Gustavsson, T., Feist, J., Kvarnström, B., & Lönroth, E. (2023). On
  business adoption and use of reproducible builds for open and closed
  source software. _Software Quality Journal_, _31_(3), 687–719.
  <https://doi.org/10.1007/s11219-022-09607-z>
- Fourne, M., Wermke, D., Enck, W., Fahl, S., & Acar, Y. (2023). It’s
  like flossing your teeth: On the importance and challenges of
  reproducible builds for software supply chain security. _2023 IEEE
  Symposium on Security and Privacy (SP)_, 1527–1544.
  <https://doi.org/10.1109/SP46215.2023.10179320>
- Schorlemmer, T. R., Kalu, K. G., Chigges, L., Ko, K. M., Isghair, E.
  A.-M. A., Baghi, S., Torres-Arias, S., & Davis, J. C. (2024).
  _Signing in four public software package registries: Quantity,
  quality, and influencing factors_.
  <https://arxiv.org/abs/2401.14635>
- Malka, J., Zacchiroli, S., & Zimmermann, T. (2024). _Reproducibility
  of build environments through space and time_.
  <https://arxiv.org/abs/2402.00424>
- Randrianaina, G. A., Khelladi, D. E., Zendra, O., & Acher, M.
  (2024). <span class="nocase">Options Matter: Documenting and Fixing
  Non-Reproducible Builds in Highly-Configurable Systems</span>.
  _<span class="nocase">MSR 2024 - 21th International Conference on
  Mining Software Repository</span>_, 1–11.
  <https://inria.hal.science/hal-04441579>
